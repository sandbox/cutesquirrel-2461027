<?php
/**
 * @file
 * KeyCDN Module.
 *
 * Drush tasks.
 */

/**
 * Implements hook_drush_cache_clear().
 */
function keycdn_drush_cache_clear(&$types) {
  $types['keycdn'] = 'keycdn_flush_cache_keycdn';
}
