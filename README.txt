-- SUMMARY --

The keycdn module uses the KeyCDN API to interact with the flush features.
It's dedicated to the KeyCDN service : http://www.keycdn.com

It allows you to flush a list of URLs or the whole zone on your KeyCDN account.

A tiny administration UI allows you to set your authentication token (can be found on keycdn.com, in your account settings).

For a full description of the module, visit the project page:
  http://drupal.org/project/keycdn

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/keycdn


-- REQUIREMENTS --

This modules requires the "CDN" module.
(it works without it, but it doesn't make sense...)

-- INSTALLATION --

* Install the module as usual, see http://drupal.org/node/895232 for further information.
* download the KeyCDN PHP API in your libraries path : https://github.com/keycdn/php-keycdn-api
 The target directory must be named "keycdn-api" :
 ex : sites/all/libraries/keycdn-api

(you should have the content of the api inside this directory : src, tests, composer.json, README.md)

-- CONFIGURATION --

* Enable the module
* Configure the permissions
* Go to the settings : admin/config/development/cdn/keycdn_settings
* Set your authentication token (can be found in your account information, on keycdn.com)
* SAVE a first time
* Choose your zone : this zone is the one you want to use with your drupal website.
Every time you flush a cache, it is flushed on this zone.
* SAVE a second time

-- API usage --

All the prefixed by "keycdn_api_" methods are used to call the API.
They can be used to retrieve zones, aliases, flush a list of URLs or flush the whole cache.

-- CONTACT --

Current maintainers:
* Etienne VOILLIOT - https://www.drupal.org/user/1480476
